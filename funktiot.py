import os
import math

def changeBit(bit): # Vaihdetaan bitti toiseksi
	if bit: # Jos bitti on 1
		bit = 0
	else:
		bit = 1
	return bit

def arrayMultiplication(G,m): # Kerrotaan binaarijono toisella kuin kerrottaisiin polynomeja
	result = [0] * 15
	
	for i in range(len(G)): # Jokaisessa kohdassa missä G on 1
		if G[i]==1:
			for j in range(len(m)): # Jokaisessa kohdassa missä m on 1
				if m[j]==1:
					result[j+i] += 1 # Plussataan G:n ja m:n potenssit yhteen
									# ja lisätään siihen kohtaan result muuttujaan +1
	for i in range(len(result)):
		result[i] = result[i] % 2 # Poistetaan parilliset potenssit
		
	return result

def thingSwitch(i): # haetaan 4 bittiset vastineet eri potensseille
	result = [0] * 4
	if i == 0:
		result[0] += 1
	elif i == 1:
		result[1] += 1
	elif i == 2:
		result[2] += 1
	elif i == 3:
		result[3] += 1
	elif i == 4:
		result[0] += 1
		result[1] += 1
	elif i == 5:
		result[1] += 1
		result[2] += 1
	elif i == 6:
		result[2] += 1
		result[3] += 1
	elif i == 7:
		result[0] += 1
		result[1] += 1
		result[3] += 1
	elif i == 8:
		result[0] += 1
		result[2] += 1
	elif i == 9:
		result[1] += 1
		result[3] += 1
	elif i == 10:
		result[0] += 1
		result[1] += 1
		result[2] += 1
	elif i == 11:
		result[1] += 1
		result[2] += 1
		result[3] += 1
	elif i == 12:
		result[0] += 1
		result[1] += 1
		result[2] += 1
		result[3] += 1
	elif i == 13:
		result[0] += 1
		result[2] += 1
		result[3] += 1
	elif i == 14:
		result[0] += 1
		result[3] += 1
	return result

def thingSwitchBackwards(i): # haetaan potenssivastine neljästä bitistä
	result = 999
	if (i[0] == 0) and (i[1] == 0) and (i[2] == 0) and (i[3] == 0):
		result = 15
	elif (i[0] == 1) and (i[1] == 0) and (i[2] == 0) and (i[3] == 0):
		result = 0
	elif (i[0] == 0) and (i[1] == 1) and (i[2] == 0) and (i[3] == 0):
		result = 1
	elif (i[0] == 0) and (i[1] == 0) and (i[2] == 1) and (i[3] == 0):
		result = 2
	elif (i[0] == 0) and (i[1] == 0) and (i[2] == 0) and (i[3] == 1):
		result = 3
	elif (i[0] == 1) and (i[1] == 1) and (i[2] == 0) and (i[3] == 0):
		result = 4
	elif (i[0] == 0) and (i[1] == 1) and (i[2] == 1) and (i[3] == 0):
		result = 5
	elif (i[0] == 0) and (i[1] == 0) and (i[2] == 1) and (i[3] == 1):
		result = 6
	elif (i[0] == 1) and (i[1] == 1) and (i[2] == 0) and (i[3] == 1):
		result = 7
	elif (i[0] == 1) and (i[1] == 0) and (i[2] == 1) and (i[3] == 0):
		result = 8
	elif (i[0] == 0) and (i[1] == 1) and (i[2] == 0) and (i[3] == 1):
		result = 9
	elif (i[0] == 1) and (i[1] == 1) and (i[2] == 1) and (i[3] == 0):
		result = 10
	elif (i[0] == 0) and (i[1] == 1) and (i[2] == 1) and (i[3] == 1):
		result = 11
	elif (i[0] == 1) and (i[1] == 1) and (i[2] == 1) and (i[3] == 1):
		result = 12
	elif (i[0] == 1) and (i[1] == 0) and (i[2] == 1) and (i[3] == 1):
		result = 13
	elif (i[0] == 1) and (i[1] == 0) and (i[2] == 0) and (i[3] == 1):
		result = 14
	return result
	
def xCalc(beta): # haetaan nollakohdat x1:lle ja x2:lle
	result = [0] * 2
	if beta == 0:
		result[0] = 0
		result[1] = 1
	elif beta == 1:
		result[0] = 6
		result[1] = 13
	elif beta == 2:
		result[0] = 11
		result[1] = 12
	elif beta == 4:
		result[0] = 7
		result[1] = 9
	elif beta == 5:
		result[0] = 2
		result[1] = 8
	elif beta == 8:
		result[0] = 3
		result[1] = 14
	elif beta == 10:
		result[0] = 1
		result[1] = 4
	return result

def s1Calc(r): # Lasketaan s1
	result = [0] * 4
	for i in range(len(r)): # Käydään läpi binaarijono
		if r[i]: # Jos kohdassa on 1,
			x = thingSwitch(i) # haetaan 4 bittiset vastineet kyseiselle potenssille
			for j in range(len(x)): # Lisätään äsken haetut 4 bittiä result muuttujaan
				if x[j]:
					result[j] += 1
					
	for i in range(len(result)): # Poistetaan parilliset potenssit
		result[i] = result[i] % 2
	
	return result
	
def s3Calc(r): # Lasketaan s3 samalla tavalla kuin s1...
	result = [0] * 4
	for i in range(len(r)):
		if r[i]:
			x = thingSwitch((i*3) % 15) # ...paitsi haettaessa bittejä kerrotaan potenssi kolmella (alfa^x^3) ja otetaan siitä modulo 15
			for j in range(len(x)):
				if x[j]:
					result[j] += 1
					
	for i in range(len(result)): # Poistetaan parilliset potenssit
		result[i] = result[i] % 2
		
	return result

