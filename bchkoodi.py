import os
import math
import numpy as np
import funktiot as f # Omakirjoittamia funktioita

print() # Tehdään vähän tilaa :)
print()

whatDoYouWant = 0 # Alustetaan valikkomuuttuja

Ginput = [0] * 10 # Alustetaan generoijapolynomi liian pitkäksi
while len(Ginput)>9: # Katsotaan ettei tehdä liian pitkää inputtia
	Ginput = input("Lisää generoijapolynomi G binaarijonona: ")	# Kysytään inputtia
	G = [0] * len(Ginput) # Tehdään inputin pituiset vektorit
	for i in range(len(Ginput)): # Täytetään vektori inputilla
		G[i] = int(Ginput[i])
	if len(Ginput)>9:
		print("Liian pitkä generoijapolynomi")
	
while whatDoYouWant != "1" and whatDoYouWant != "2": # Jos valinta on väärin niin kysytään uudelleen
	whatDoYouWant = input("Paina 1 jos haluat koodata, 2 jos haluat dekoodata")
	
	if whatDoYouWant == "1": # Valittu koodaus
	
		minput = [0] * 8
		while len(minput)>7: # Katsotaan ettei tehdä liian pitkää inputtia
			minput = input("Lisää sana m binaarijonona: ") # Kysytään inputtia
			
			m = [0] * len(minput) # Tehdään inputin pituiset vektorit
			
			for i in range(len(minput)): # Täytetään vektori inputilla
				m[i] = int(minput[i])
			if len(minput)>7:
				print("Liian pitkä sana")
		
		c = f.arrayMultiplication(G,m) # Kerrotaan generoijapolynomi ja sana keskenään
			
		print("Koodattu c: ", end="") # Tulostetaan koodattu viesti
		for i,num in enumerate(c):
			print(num, end="")
			if ((i+1) % 3) == 0:
				print(" ", end="")

			
	elif whatDoYouWant == "2": # Valittu dekoodaus
	
		rinput = [0]*16
		while len(rinput)>15: # Katsotaan ettei tehdä liian pitkää inputtia
			rinput = input("Lisää viesti r binaarijonona: ") # Kysytään inputtia
			r = [0] * len(rinput) # Tehdään inputin pituiset vektorit
			for i in range(len(rinput)): # ja lisätään inputti vektoriin
				r[i] = int(rinput[i])
			if len(Ginput)>15:
				print("Liian pitkä viesti")
		
		s1 = f.thingSwitchBackwards(f.s1Calc(r)) # Lasketaan syndromi S1 ja muutetaan se yhdeksi polynomiksi
		s3 = f.thingSwitchBackwards(f.s3Calc(r)) # Sama mutta S3
		if s1 == 999 and s3 == 999: # Jos ei osuttu mihinkään mahdolliseen vaihtoehtoon
			print("s1 ja s3 laskettiin väärin") # niin tulostetaan että meni väärin ja hypätään ulos silmukasta
			break
		elif s1 == 999: 
			print("s1 laskettiin väärin")
			break
		elif s3 == 999: 
			print("s3 laskettiin väärin")
			break
		elif s1 == 15 and s3 == 15: # Jos S1 = 0 tai S2 = 0 (numerona 15 koska numerot ovat potensseja ja 0 on käytössä)
			print("Viesti ei ole muuttunut, r: ", end="")
			for i,num in enumerate(r): # tulostetaan alkuperäinen viesti ja hypätään ulos silmukasta
				print(num, end="")
				if ((i+1) % 3) == 0:
					print(" ", end="")
			break
		elif (s1*3) % 15 == s3: # jos S1^3 = 3
			r[s1] = f.changeBit(r[s1]) # Vaihdetaan bitti kohdassa S1
		else:
			if s1 == 15: # Tehdään varmistuksia että "väärin kirjoitettu" 0 ei mene läpi
				beta = s3 % 15
			elif s3 == 15:
				beta = (0-s1*3) % 15
			else:
				beta = (s3 - s1*3) % 15 # Lasketaan betan potenssi
			
			x = f.xCalc(beta) # Käytetään funktiota joka laskee nollakohdat
			if x[0]==0 and x[1]==0: # Jos ei nollakohtia
				print("Enemmän kuin 2 virhettä")
			else: # Jos 2 virhettä eikä mitään väärin
				s1x1 = (s1+x[0]) % 15 # s1 ja x1 potenssit lisätään yhteen
				s1x2 = (s1+x[1]) % 15 # s1 ja x2 potenssit lisätään yhteen
				r[s1x1] = f.changeBit(r[s1x1]) # Vaihdetaan s1x1 kohdassa oleva bitti viestistä r
				r[s1x2] = f.changeBit(r[s1x2]) # Vaihdetaan s1x2 kohdassa oleva bitti viestistä r

				print()
					
		print("c*: ", end="") # Tulostetaan korjattu viesti
		for i,num in enumerate(r):
			print(num, end="")
			if ((i+1) % 3) == 0: # Tehdään välit kolmen numeron välein
				print(" ", end="")
				
		x = np.array(r) # Tehdään r:stä ja G:stä "polynomit"
		y = np.array(G)
		m = np.polydiv(x, y) # c*/G
		
		print()
		
		print("m*: ", end="")
		for i,num in enumerate(m[0]): # Tulostetaan koodattu sana
			print(abs((int(num)) % 2), end="") # Tulostetaan jokainen numero itseisarvona modulo 2 jotta saadaan miinusmerkit pois ja luku binaariksi
			if ((i+1) % 3) == 0:				# 
				print(" ", end="")
				
	else: # jos valikkonumero oli jotain muuta kun 1 tai 2
		print("Väärä numero")
		
print()
print()
print()
os.system('pause')